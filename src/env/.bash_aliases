alias ls="ls -h --color=tty"
alias ll='ls -l'
alias la='ls -A'
alias e='$EDITOR'
alias tmo='tmux attach'
alias du='du -h'
alias cp='cp -i'
alias mv='mv -i'
alias fotomv='exiv2 -t -F mv'

