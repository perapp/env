#!/bin/bash

set -e

# run install again and assert that .bashrc is backed up
~/.env/install.sh
if ! test -f ~/.bashrc.org; then
   echo "ERROR .bashrc not backed up!" >&2
   exit 1
fi
