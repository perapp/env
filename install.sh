#!/bin/bash

set -e

function run_installer() {
    installer="$1"
    file_pattern="$2"

    [ -x "$installer" ] || return 0

    for file in $(compgen -G "$file_pattern"); do
        while read line; do
            sudo "$installer" $line || return 1
        done < "$file"
    done

    return 0
}

ENV_HOME=$(readlink -f $(dirname "$0"))
if [ -e /etc/os-release ]; then
    source <(grep = /etc/os-release | sed s/^/OS_/)
fi

for lname in $(ls -A "$ENV_HOME/src/env"); do
    ltarget="$ENV_HOME/src/env/$lname"
    lpath="$HOME/$lname"
    [ "$ltarget" -ef "$lpath" ] && continue
    if [ -e "$lpath" -a ! -e "$lpath.org" ]; then
        mv "$lpath" "$lpath.org"
    fi
    echo "$lname"
    ln -nfs "$ltarget" "$lpath"
done

if [ "$1" = -p ]; then
    OS_HOME="$ENV_HOME/src/packages/$OS_ID"
    if [ -d "$OS_HOME" ]; then

        echo -e "\nINSTALL $OS_NAME packages..."
        run_installer "$OS_HOME/install_repo" "$OS_HOME/*.repo"
        run_installer "$OS_HOME/install_pkg" "$OS_HOME/*.pkg"

        echo -e "\nINSTALL Python packages..."
        PY_HOME="$ENV_HOME/src/packages/python"
        run_installer "$PY_HOME/install_pip" "$PY_HOME/*.pip"
        run_installer "$PY_HOME/install_pip3" "$PY_HOME/*.pip3"
    fi
fi
