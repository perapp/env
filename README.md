# env
Version controlled shell envioronment for Linux systems.

## About
This small package allows you to keep version control of settings for
Bash aliases, Emacs configuration, Tmux settings, etc.
It provides an easy way to set them up on all computers
you work on like cloud servers, office laptop, home computer.

It also provides a way to define a set of packages to install on all your
computers.

To use it, start with forking this project and then follow the install
instructions below.

## How to install

To install run:
```
git clone https://gitlab.com/perapp/env.git .env
.env/install.sh -p

sudo systemctl start docker
sudo usermod -aG docker $USER
newgrp docker 
```
